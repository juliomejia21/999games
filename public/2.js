(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[2],{

/***/ "./resources/js/EmployeeDashboardPopups.js":
/*!*************************************************!*\
  !*** ./resources/js/EmployeeDashboardPopups.js ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_1__);


/* harmony default export */ __webpack_exports__["default"] = (function () {
  if (document.querySelector('.js-stop-timer')) {
    var $stopTimerButton = document.querySelectorAll('.js-stop-timer');
    $stopTimerButton.addEventListener('click', function (e) {
      e.preventDefault();
      var value = e.currentTarget.dataset.value;
      var url = e.currentTarget.closest('.round__edit').dataset.url;
      sweetalert2__WEBPACK_IMPORTED_MODULE_0___default.a.fire({
        title: 'Let op!',
        text: 'Weet u zeker dat u de ronde vroegtijdig wilt stoppen?',
        icon: 'warning',
        confirmButtonText: 'Ja',
        showCancelButton: true,
        cancelButtonText: 'Nee',
        showCloseButton: true,
        preConfirm: function preConfirm() {
          axios__WEBPACK_IMPORTED_MODULE_1___default.a.put(url, {
            status: value
          }); // axios.put(url, {status: value}).then((response) => {
          //     location.reload();
          // });
        }
      });
    });
  }

  if (document.querySelector('.js-start-round')) {
    var $startRoundButtons = document.querySelectorAll('.js-start-round');
    $startRoundButtons.forEach(function (button) {
      button.addEventListener('click', function (e) {
        e.preventDefault();
        var value = e.currentTarget.dataset.value;
        var url = e.currentTarget.closest('.round__edit').dataset.url;
        sweetalert2__WEBPACK_IMPORTED_MODULE_0___default.a.fire({
          title: 'Let op!',
          text: 'Zit iedereen klaar?',
          icon: 'warning',
          confirmButtonText: 'Ja',
          showCancelButton: true,
          cancelButtonText: 'Nee',
          showCloseButton: true,
          preConfirm: function preConfirm() {
            axios__WEBPACK_IMPORTED_MODULE_1___default.a.put(url, {
              status: value
            }).then(function (response) {
              // console.log(response);
              location.reload();
            })["catch"](function (error) {// console.log(error, 'err');
            });
          }
        });
      });
    });
  }
});

/***/ })

}]);