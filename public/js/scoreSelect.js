/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 2);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/scoreSelect.js":
/*!*************************************!*\
  !*** ./resources/js/scoreSelect.js ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

var formDOM = document.getElementsByClassName("player-scores")[0];
var tableDOM = document.getElementById("table");
tableDOM.addEventListener("change", getPlayerNames);

function getPlayerNames() {
  var table = tableDOM.value;
  fetch("/" + 1 + "/employee/" + table + "/round/players", {
    method: 'get'
  }).then(function (response) {
    return response.json();
  }).then(function (json) {
    InsertPlayers(json);
  });
}

function clearPlayerDOM() {
  var DOMList = Array.from(document.getElementsByClassName("player-score-group"));
  DOMList.forEach(function (element) {
    element.remove();
  });
}

function InsertPlayers(playerList) {
  clearPlayerDOM();
  playerList.forEach(function (player) {
    var inputGroupDOM = document.createElement("div");
    inputGroupDOM.classList.add("form-group", "player-score-group");
    formDOM.appendChild(inputGroupDOM);
    var labelDOM = document.createElement("label");
    var inputDOM = document.createElement("input");
    inputGroupDOM.appendChild(labelDOM);
    inputGroupDOM.appendChild(inputDOM);
    labelDOM.classList.add("label");
    labelDOM.innerText = player.name + " score:";
    inputDOM.classList.add("form-control");
    inputDOM.type = "number";
    inputDOM.name = "players[" + player.id + "]";
  });
}

/***/ }),

/***/ 2:
/*!*******************************************!*\
  !*** multi ./resources/js/scoreSelect.js ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\Jelle\Documents\School\ProjectFrameworks\resources\js\scoreSelect.js */"./resources/js/scoreSelect.js");


/***/ })

/******/ });