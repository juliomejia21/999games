<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Tournament;
use App\TournamentUser;
use Faker\Generator as Faker;
use Illuminate\Foundation\Auth\User;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(TournamentUser::class, function (Faker $faker) {
    return [
        'user_id' => factory('App\User')->create()->id,
        'tournament_id' => Tournament::all()->random()->id,
        'status' => 0,
        'qr_token' => (string) Str::uuid(),
        'weight' => 0
    ];
});
