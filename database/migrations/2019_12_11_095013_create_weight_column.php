<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWeightColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_round_table', function (Blueprint $table) {
            $table->unsignedBigInteger("weight")->nullable();
        });
        Schema::table('tournament_user', function (Blueprint $table) {
            $table->unsignedBigInteger("weight")->nullable();
        });
    }

    /**w
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_round_table', function (Blueprint $table) {
            $table->dropColumn('weight');
        });
        Schema::table('tournament_user', function (Blueprint $table) {
            $table->dropColumn('weight');
        });
    }
}
