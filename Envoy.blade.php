@servers(['web' => 'julio@adsd2019.clow.nl'])

@setup
$gitUrl = 'git@gitlab.com:juliomejia21/999games.git';
$branch = (!empty($branch)) ? $branch : 'master';
$basePath = '/home/julio/public_html/999games';
@endsetup

@task('deploy:cold')
cd {{ $basePath }}
git init
git remote add origin {{ $gitUrl }}
git fetch
git checkout {{ $branch }}
git pull origin {{ $branch }}
composer install
npm install
npm run dev
php artisan storage:link
php artisan migrate
chmod -R 777 storage
@endtask

@task('deploy')
cd {{ $basePath }}
git fetch
git checkout {{ $branch }}
git pull origin {{ $branch }}
composer install
npm install
npm run dev
php artisan migrate
chmod -R 777 storage
@endtask

