<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Round extends Model
{
    protected $fillable = ['tournament_id','deadline', 'status', 'name'];

    public function users(){
        return $this->belongsToMany(User::class,"user_round_table")->withPivot('table_id');
    }

    public function tournament(){
        return $this->belongsTo(Tournament::class);
    }

    public function tables(){
        return $this->belongsToMany(Table::class,"user_round_table");
    }

    public function userRoundTable()
    {
        return $this->hasMany(UserRoundTable::class);
    }

    public function emptyScores() {
        $collection = $this->hasMany(UserRoundTable::class);
        $filtered = $collection->where('score', '=', 0)
            ->pluck('score', 'user_id');
        return $filtered;
    }
    public function score(){
        return $this->hasMany(Score::class);
    }

}
