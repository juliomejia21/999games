<?php

namespace App\Http\Controllers;

use App\Mail\sending_qr;
use App\Tournament;
use App\TournamentUser;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class TournamentUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allTournaments = Tournament::get();
        $tournaments = Tournament::get();
        $signups = Auth::user()->signups;

        $tournaments = $tournaments->reject(function ($tournament) {
            return $tournament->signups()->where('user_id', Auth::id())->first() || $tournament->date <= date("Y-m-d");
        });

        $SelectTournaments = $tournaments->pluck('name', 'id');

        return view('tournamentsignup.index', compact('signups', 'tournaments', 'SelectTournaments', 'allTournaments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $check = TournamentUser::where(['tournament_id' => $request->tournament_id, 'user_id' => Auth::id()])->first();
        $token = (string) Str::uuid();
        $tournament = Tournament::where('id', $request->get('tournament_id'))->value('name');

        if ($check !== null) {
            return redirect()->back()->withErrors(['SignedinTournament' => 'Je bent al aangemeld voor dit toernooi']);
        }

        TournamentUser::create(
            [
                'qr_token' =>  $token,
                'tournament_id' => $request->tournament_id,
                'user_id' => Auth::id(),
                'weight' => 0
            ]
        );

        $qr = [
            'qrbash' => base64_encode(QrCode::format('png')->size(600)->errorCorrection('H')->generate($token)),
            'tournament' => $tournament
        ];

        \Mail::to(Auth::user()->email)->Queue(new sending_qr($qr));
//        return redirect()->route('tournament-signup.index');
        return redirect()->back()->withErrors(['Er is een qr code gemailed om in te checken bij het tournament.']);

    }

    /**
     * Display the specified resource.
     *
     * @param \App\TournamentUser $tournamentUser
     * @return \Illuminate\Http\Response
     */
    public function show(TournamentUser $tournamentUser)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\TournamentUser $tournamentUser
     * @return \Illuminate\Http\Response
     */
    public function edit(TournamentUser $tournamentUser)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\TournamentUser $tournamentUser
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TournamentUser $tournamentUser)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\TournamentUser $tournamentUser
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, TournamentUser $signup)
    {
        if ($signup->user_id === Auth::id()) {
            $signup->delete();
            return redirect()->back()->withErrors(['SignedoffTournament' => 'Je bent succesvol afgemeld voor het toernooi']);
        } else {
            return redirect()->back();
        }

    }
}
