<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    public function redirectTo(){

        // User role
        $user = Auth::user();
        if ($user->hasRole('admin')) return '/tournament/employee/dashboard';
        if ($user->hasRole('medewerker')) return '/tournament/employee/dashboard';
        return '/tournament/signup';
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed',  'regex:/[A-Z]/','regex:/[a-z]/','regex:/[0-9]/'],
            'telefoonnummer' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
       ]
        );
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user = config('roles.models.defaultUser')::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'telefoonnummer'=> $data['telefoonnummer'],
            'password' => Hash::make($data['password']),
        ]);
        $role = config('roles.models.role')::where('name', '=', 'Speler')->first();  //choose the default role upon user creation.
        $user->attachRole($role);

        return $user;
    }
}
