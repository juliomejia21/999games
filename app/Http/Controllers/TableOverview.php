<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use DB;
use App\Round;
use App\UserRoundTable;
use App\User;
use App\TournamentUser;
use App\RoundTable;

class TableOverview extends Controller
{
    public function show($tournament) {

        $latestRound = Round::where('tournament_id', '=',  $tournament)->orderBy('name' ,'desc')->get('id')->first();

        if(!empty($latestRound)) {
            $tables = Round::join('user_round_table', 'rounds.id', '=', 'user_round_table.round_id')
                ->where([['rounds.id', '=', $latestRound->id], ['rounds.tournament_id', '=', $tournament]])
                ->select('user_round_table.table_id')
                ->distinct()
                ->get();

            $users = UserRoundTable::join('tournament_user', 'user_round_table.user_id', '=', 'tournament_user.user_id')
                ->join('users', 'tournament_user.user_id', '=', 'users.id')
                ->select('users.name', 'user_round_table.table_id')
                ->where([['user_round_table.round_id', '=', $latestRound->id], ['tournament_user.tournament_id', '=', $tournament], ['tournament_user.status', '=', 1]])
                ->get();

        } else {
            $tables = "";
            $users = "";
        }

        return view('tableOverview',
            [
                'users' => $users,
                'tables' => $tables
            ]
        );
    }
}
