<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreScoreRequest;
use App\Mail\sending_result;
use App\Round;
use App\Score;
use App\Table;
use App\Tournament;
use App\User;
use Facade\Ignition\Tabs\Tab;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use jeremykenedy\LaravelRoles\Models\Role;

class ScoreController extends Controller
{
    public function mail(Tournament $tournament)
    {
        $admins = Role::where('slug', 'admin')->first()->users()->get();
        foreach ($admins as $admin){
            \Mail::to($admin)->Queue(new sending_result($tournament));
        }
        return redirect()->back()->withErrors(['email_success' => 'De mail is verstuurd.']);
    }

    public function calculateWeight(Table $table, Round $round)
    {
        $users = $table->users($round)->withPivot(["weight"])->get();
        $scores = [];
        foreach ($users as $user) {
            $scores[$user->pivot->user_id] = ["score" => $user->scoreInRound($round)->first()->score];
        }
        $totalScore = array_sum(array_column($scores, "score"));
        foreach ($scores as $key => $score) {
            $user = $users->find($key);
            $user->pivot->weight = (int)round(($score["score"] / $totalScore) * 100);
            $user->pivot->save();
        }
    }

    public function assignTournamentPoints(Table $table, Round $round){
        $users = $table->users($round)->withPivot(["score","tournament_points"])->orderByDesc("score")->get();
        $points = [13,9,5,1];
        foreach ($users as $key => $user){
            $user->pivot->tournament_points = $points[$key];
            $user->pivot->save();
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Tournament $tournament)
    {
        $tables = $tournament->rounds()->orderByDesc("id")->firstOrFail()->tables()->get();
        $formList = [];
        foreach ($tables as $table) {
            $formList[$table->id] = $table->name;
        }
        return view("employee.score", ["tables" => $formList, "tournament" => $tournament]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreScoreRequest $request, Tournament $tournament)
    {
        $round = $tournament->rounds()->latest("id")->first();
        foreach ($request->players as $playerId => $score){

            User::findOrFail($playerId)->score()->first()->updateOrCreate(["round_id"=>$round->id,'user_id'=>$playerId],["score"=>$score]);
            User::findOrFail($playerId)->score()->first()->updateOrCreate(["round_id"=>$round->id,'user_id'=>$playerId],["score"=>$score]);
        }
        $this->calculateWeight(Table::find($request->table),$round);
        $this->assignTournamentPoints(Table::find($request->table),$round);
        return redirect()->back()->with("message",["type"=>"success","content"=>"Scores succesvol ingevuld."]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Score $score
     * @return \Illuminate\Http\Response
     */
    public function show(Score $score)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Score $score
     * @return \Illuminate\Http\Response
     */
    public function edit(Score $score)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Score $score
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Score $score)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Score $score
     * @return \Illuminate\Http\Response
     */
    public function destroy(Score $score)
    {
        //
    }
}
