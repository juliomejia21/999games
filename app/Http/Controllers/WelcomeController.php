<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tournament;
use Carbon\Carbon;

class WelcomeController extends Controller
{
    //

    public function show() {
        $tournaments = Tournament::select('name', 'id', 'date')
            ->latest('id')
            ->orderBy('date')
            ->get();

        $first_joinable_tournament = "";

        foreach ($tournaments as $tournament) {
            if ($tournament->date >= Carbon::now()) {
                $first_joinable_tournament = $tournament;
            }
        }

        return view('welcome', [
            'tournament' => $first_joinable_tournament
        ]);
    }
}
