<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTournamentRequest;
use App\Tournament;
use DateTime;
use Illuminate\Http\Request;

class TournamentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    public function timer(Tournament $tournament)
    {
        return view('timer', ['tournament'=>$tournament]);
    }

    public function gettime(Tournament $tournament)
    {
        $currentround = $tournament->rounds()->where('status','inprogress')->first();
        if ($currentround) {
            $deadline = $currentround->deadline;
            if ($deadline == null) {
                $deadline = new DateTime(now());
                //Game time
                $deadline->modify('+45 minutes');
                //Countdown before game
                $deadline->modify('+10 seconds');
                $currentround->update(['deadline' => $deadline]);
            }
            $datetime = strtotime($tournament->rounds()->where('status', 'inprogress')->first()->deadline);
        }else{
            $datetime = mktime(10, 9, 0, 12, 11, 2019);
        }

        return date('D M d Y H:i:s O',$datetime);
    }

//    public function stoptime(Tournament $tournament) {
//
//    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tournament.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateTournamentRequest $request)
    {
        Tournament::create($request->validated());

        return redirect()->route('home');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Tournament $tournament
     * @return \Illuminate\Http\Response
     */
    public function show(Tournament $tournament)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Tournament $tournament
     * @return \Illuminate\Http\Response
     */
    public function edit(Tournament $tournament)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Tournament $tournament
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tournament $tournament)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Tournament $tournament
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tournament $tournament)
    {
        //
    }
}
