<?php

namespace App\Http\Controllers;

use App\GeneralRanking;
use App\Round;
use App\Table;
use App\Tournament;
use App\TournamentUser;
use App\User;
use App\Score;
use App\UserRoundTable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GeneralRankingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Tournament $tournament)
    {
        $qr_token = TournamentUser::where(
            [
                ['tournament_id', $tournament['id']],
                ['user_id', Auth::id()]
            ]
        )->value('qr_token');
        $generalRankingList = $this->GeneralRankingList($tournament);
        $UserRoundScore = $this->UserScorePerRonde($tournament);

//       return $UserRoundScore;
     return view('algemeneRanglijst', compact('generalRankingList','UserRoundScore', 'qr_token'));
    }


    public function GeneralRankingList(Tournament $tournament)
    {
        $users = $tournament->users;
        $users->map(function ($user){
            $result = 0;
                    foreach ($user->userRoundTable()->get() as $score){
                $result += $score->weight;
            }
           $user->score = $result;
           return $user;
        });
        $users = $users->sortByDesc('score');
        return $users;
    }

    /**
     * @param Tournament $tournament
     */
    public function UserScorePerRonde(Tournament $tournament)
    {
        $user = $tournament->users()->find(Auth::user());
        $user = $user->userRoundTable;
        return ($user);
    }
}

