<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateStatus;
use App\Http\Requests\UpdateStatusRequest;
use App\Round;
use App\Table;
use App\Tournament;
use App\User;
use App\UserRoundTable;
use Illuminate\Http\Request;

class RoundController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Tournament $tournament)
    {
        if(!$tournament->users()->count()) {
            // als er geen users zijn, een error teruggeven

            return redirect()->back()
                ->withErrors(['users' => 'Er zijn geen gebruikers om een ronde mee aan te maken!']);
        } else if(Round::latest()->first() && Round::latest()->first()->emptyScores()->count()) {
            // als er op de actieve ronde nog users zijn die geen score hebben mag die geen nieuwe ronde aanmaken

            return back();
        } else if(!$tournament->rounds->count()) {
            //als het tournooi nog geen rondes heeft moet de eerste ronde aangemaakt worden

            $tournamentUsers = $tournament->users;

            $round = Round::create([
                'name' => 1,
                'tournament_id' => $tournament->id
            ]);

            $numberOfGroups = ceil($tournamentUsers->count() / 4);
            $groups = $tournamentUsers->split($numberOfGroups);

            $table_id = 1;

            foreach($groups as $group) {
                $table = Table::create([
                    'name' => 'tafel ' . $table_id
                ]);

                foreach($group as $user) {
                    UserRoundTable::create([
                        'round_id' => $round->id,
                        'user_id' => $user->id,
                        'table_id' => $table->id,
                    ]);
                }

                $table_id += 1;
            }
        } else if($tournament->rounds->count() === 1) {
            // als het tournooi één ronde heeft moeten de tafels aangemaakt worden op het gewicht
            // van de score van hoog naar laag

            $tournamentUsers = Tournament::first()->rounds()->with(array('userRoundTable' => function($query)
                    { $query->orderBy('weight', 'DESC'); }
                ))->first()->userRoundTable;

            $round = Round::create([
                'name' => Round::latest()->first() ? Round::latest()->first()->name += 1 : 1,
                'tournament_id' => $tournament->id
            ]);

            $numberOfGroups = ceil($tournamentUsers->count() / 4);
            $groups = $tournamentUsers->split($numberOfGroups);

            $table_id = 1;

            foreach($groups as $group) {
                $table = Table::create([
                    'name' => 'tafel ' . $table_id
                ]);

                foreach($group as $user) {
                    UserRoundTable::create([
                        'round_id' => $round->id,
                        'user_id' => $user->user_id,
                        'table_id' => $table->id,
                    ]);
                }

                $table_id += 1;
            }
        } else if($tournament->rounds->count() > 1) {
            // als het tournooi al meerdere rondes heeft moet het gemiddelde gewicht van de speler op basis
            // van het gewicht van elke ronde berekend worden en aan de hand daarvan moeten de
            // gemiddelde spelergewichten van hoog naar laag weer ingedeeld worden in rondes

            $averageUserWeight = [];

            foreach($tournament->users as $user) {
                $totalUserWeight = null;

                foreach($user->scores as $row) {
                    $totalUserWeight += $row->weight;
                }

                $weight = $totalUserWeight / $tournament->rounds()->count();

                $averageUserWeight[] = array('weight' => $weight, 'id' => $user->id);
            }

            $allUsersWeight = collect($averageUserWeight)->sortByDesc('weight');

            $round = Round::create([
                'name' => Round::latest()->first() ? Round::latest()->first()->name += 1 : 1,
                'tournament_id' => $tournament->id
            ]);

            $numberOfGroups = ceil($allUsersWeight->count() / 4);
            $groups = $allUsersWeight->split($numberOfGroups);

            $table_id = 1;

            foreach($groups as $group) {
                $table = Table::create([
                    'name' => 'tafel ' . $table_id
                ]);

                foreach($group as $user) {
                    UserRoundTable::create([
                        'round_id' => $round->id,
                        'user_id' => $user['id'],
                        'table_id' => $table->id,
                    ]);
                }

                $table_id += 1;
            }
        }

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\round  $round
     * @return \Illuminate\Http\Response
     */
    public function show(round $round)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\round  $round
     * @return \Illuminate\Http\Response
     */
    public function edit(round $round)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateStatus $request
     * @param Tournament $tournament
     * @param \App\round $round
     * @return void
     */
    public function update(UpdateStatus $request, Tournament $tournament, round $round)
    {
        $validated = $request->validated();

//        $currentRound = $tournament->rounds()->where('status','inprogress')->first();

        $round->deadline = null;

        $round->update(['status' => $validated['status']]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\round  $round
     * @return \Illuminate\Http\Response
     */
    public function destroy(round $round)
    {
        //
    }
}
