<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Score extends Model
{
    protected $fillable = ["user_id","round_id","score"];

    public function users(){
        return $this->belongsTo("App\User");
    }

    public function round(){
        return $this->belongsTo("App\Round");
    }
    public function tournament(){
        return $this->belongsTo("App\Tournament");
    }
}

