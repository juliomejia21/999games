<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Tournament extends Model
{
    protected $fillable = ['name', 'date'];

    public function rounds()
    {
        return $this->hasMany(Round::class);
    }

    public function users() {
        return $this->belongsToMany(User::class, 'tournament_user')->withTimestamps()->withPivot('id');
    }

    public function signups()
    {
        return $this->hasMany(TournamentUser::class);
    }

    public static function nextTournament()
    {
        return Tournament::where('date', '>=', Carbon::now())->orderBy('date')->first();
    }
    public function score(){
        return $this->hasMany(Score::class);
    }
}
