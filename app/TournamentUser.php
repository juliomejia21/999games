<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

class TournamentUser extends Pivot
{
    protected $fillable = ['user_id', 'status', 'qr_token', 'tournament_id', 'weight'];

    public function tournament()
    {
        return $this->hasOne(Tournament::class, 'id', 'tournament_id')->first();
    }

    public function user()
    {
        return $this->hasMany(User::class, 'id');
    }
}
