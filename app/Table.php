<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Table extends Model
{
    protected $fillable = ["name"];

    public function users(Round $round){
        return $this->belongsToMany(User::class,"user_round_table")
            ->withPivot(["round_id"])
            ->where("round_id","=",$round->id);
    }
}
