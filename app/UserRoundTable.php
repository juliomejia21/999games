<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;


class UserRoundTable extends Pivot
{
    protected $fillable = ['round_id', 'table_id', 'user_id', 'weight', 'score'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function table()
    {
        return $this->belongsTo(Table::class);
    }
}

