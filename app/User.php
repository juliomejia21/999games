<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use jeremykenedy\LaravelRoles\Traits\HasRoleAndPermission;

class User extends Authenticatable
{
    use Notifiable;
    use HasRoleAndPermission;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'telefoonnummer',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function tournament()
    {
        return $this->belongsToMany(Tournament::class);
    }

    public function scoreInRound(Round $round)
    {
        return $this->hasMany(UserRoundTable::class)->where("round_id", "=", $round->id);
    }

    public function scores()
    {
        return $this->hasMany(UserRoundTable::class);
    }

    public function tables()
    {
        return $this->belongsToMany(Table::class, 'user_round_table')
            ->withPivot(["round_id"]);
    }

    public function userRoundTable()
    {
        return $this->hasMany(UserRoundTable::class);
    }

    public function round()
    {
        return $this->belongsToMany(Round::class);
    }

    public function signups()
    {
        return $this->hasMany(TournamentUser::class);
    }



}
