@extends('layouts.app')

@section('content')

    <div class="container backdrop h-100">
        <div class="row h-100">
            <div class="col-sm-12 col-md-6 text-left my-auto">
                @if($tournament)
                    <h1 class="text-light">Welkom bij het {{ $tournament->name }}</h1>
                    <h2 class="text-secondary">U kunt zich aanmelden om mee te doen aan het {{ $tournament->name }}.</h2>

                    <a href="{{ route('signup.index') }}" class="btn btn-outline-light">Aanmelden</a>
                @else
                    <h1 class="text-light">Welcome bij de Carcassonne registratie website.</h1>
                    <h2 class="text-secondary">Er zijn op dit moment nog geen wedstrijden toegevoegd.</h2>
                @endif
            </div>
            <div class="col-sm-0 col-md-6 pt-4">
                <img src="https://999games.nl/carcassonne/img/homepage/basisspel-main-img.png" alt="Carcassona basisspel" class="img-fluid">
            </div>
        </div>
    </div>

    <link href="{{ asset('css/welcome.css') }}" rel="stylesheet">

@endsection
