@extends("layouts.app")

@section("content")

@foreach($errors as $error)
    $error
    @endforeach
    <div class="container">
        <div class="row">
            <div class="col-6 offset-3">
                @if (session('message'))
                    <div class="alert alert-{{session('message')['type']}}">
                        {{ session('message')['content'] }}
                    </div>
                @endif
                {!! Form::open(['method'=>'post','route'=>['score.store',$tournament]]) !!}
                    <div class="form-group">
                        {!! Form::label('table','Tafel:') !!}
                        {!! Form::select('table',$tables,null,["placeholder"=>"Selecteer een tafel","class"=>"custom-select"]) !!}
                    </div>
                    <div class="player-scores">

                    </div>
                {!! Form::submit("Invullen",['class'=>'btn btn-primary']) !!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
