@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                @if($tournamentUsers->first() != null)
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Aangemelde spelers voor het toernooi:</h4>
                        </div>
                        <div class="card-body">
                            <div class="m-1">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th scope="col">Naam</th>
                                        <th scope="col">Datum aangemeld</th>
                                        <th scope="col">Afmelden</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($tournamentUsers as $tournamentUser)
                                        <tr>
                                            <td>{{ $tournamentUser->name }}</td>
                                            <td>{{ date_format(date_create($tournamentUser->pivot->created_at),"d-m-Y") }}</td>

                                            <td>{!! Form::open(['method' => 'DELETE', 'route' => ['signups.destroy',$tournament, 1,$tournamentUser->pivot->id]]) !!}
                                                {!! Form::submit('Speler Afmelden', ['class'=>'btn btn-danger','onclick' => 'return confirm("Weet je zeker dat je deze gebruiker wilt uitschrijven?");']) !!}
                                                {!! Form::close() !!}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                <div>{{ $tournamentUsers->links() }}</div>
                            </div>
                        </div>
                    </div>
                @else
                <div class="card">
                    <div class="card-header text-center">Geen aanmeldingen voor dit toernooi</div>
                </div>

                @endif
            </div>
        </div>
    </div>
@endsection
