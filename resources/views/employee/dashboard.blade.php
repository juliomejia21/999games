@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card mb-3">
                    <div class="card-header">
                        Eind resultaat mailen naar alle Admins
                    </div>

                    <div class="card-body">
                        <div class="col-12 text-center">
                            @if($errors->has('email_success'))
                                <h4 class="alert alert-success">{{$errors->first('email_success')}}</h4>
                            @endif
                        </div>
                        <form id="logout-form" action="{{ route('score.mail', $tournament->id) }}" method="POST">
                            @csrf
                            <button type="submit" class="btn btn-success">
                                Mailen
                            </button>
                        </form>

                    </div>
                </div>
                <div class="card mb-3">
                    <div class="card-header">
                        Nieuwe ronde aanmaken
                    </div>

                    <div class="card-body">
                        <a href="{{ route("round.store", $tournament->id) }}"
                           class="btn btn-success {{ $currentRoundHasEmptyScores ? 'disabled' : '' }}"
                        >
                            Genereren
                        </a>

                        @error('users')
                        <div class="alert alert-danger mt-3" role="alert">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                </div>
                @if(!$rounds->isEmpty())
                    <div class="card">
                        <div class="card-header">
                            Rondes
                        </div>
                        <div class="card-body">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Aangemaakt op</th>
                                        <th scope="col">Ronde overzicht</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($rounds as $round)
                                        <tr class="round__edit" data-url="{{ route('round.update',  [
                                            'tournament' => $tournament->id,
                                            'round' => $round->id
                                        ]) }}">
                                            <th scope="row">{{ $round->id }}</th>
                                            <td>{{ $round->created_at }}</td>
                                            <td>
                                                @switch($round->status)
                                                    @case('inprogress')
                                                        @if($round->deadline === null)
                                                            <a href="{{ route('timer', $tournament) }}" class="btn btn-success js-start-timer">Timer Starten</a>
                                                        @else
                                                            <a href="{{ route('timer', $tournament) }}" class="btn btn-primary" >Timer bekijken</a>

                                                            <button class="btn btn-warning js-stop-timer" data-value="stopped">
                                                                Timer stoppen
                                                            </button>
                                                        @endif
                                                        @break
                                                    @case('stopped')
                                                        @if($rounds->last()->id === $round->id && $round->emptyScores()->count())
                                                            <a href="{{ route('score.create', $tournament->id) }}" class="btn btn-primary">Punten invullen</a>
                                                        @endif
                                                        <a href="{{ route('round.show', [
                                                            'tournament' => $tournament->id,
                                                            'round' => $round->id
                                                        ]) }}" class="btn btn-link">Ronde inzien</a>
                                                        @break
                                                    @default
                                                        <button class="btn btn-success js-start-round" data-value="inprogress">Ronde starten</button>
                                                @endswitch
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
