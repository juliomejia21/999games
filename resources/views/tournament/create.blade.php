@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Nieuw Tournament aanmaken</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        {{ Form::open(array('route' => 'tournament.store')) }}
                        <div class="form-group">
                            {{ Form::label('name', 'Naam')}}
                            {{ Form::text('name',null,['class'=>'form-control'])}}
                            @error('name')
                                <p class="text-danger">
                                    {{ $message }}
                                </p>
                            @enderror
                        </div>
                        <div class="form-group">
                            {{ Form::label('date', 'Datum')}}
                            {{ Form::date('date',null,['class'=>'form-control'])}}
                            @error('date')
                                <p class="text-danger">
                                    {{ $message }}
                                </p>
                            @enderror
                        </div>
                        {{Form::submit('Aanmaken',array('class'=>'btn btn-primary'))}}

                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
