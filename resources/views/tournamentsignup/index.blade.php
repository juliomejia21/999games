@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-10 text-center">
                @if($errors->any())
                    <h4 class="alert alert-success">{{$errors->first()}}</h4>
                @endif
            </div>
            <div class="col-md-10">
                @if( $SelectTournaments->first()!= null )
                    <div class="card">
                        <div class="card-header">
                            Je kan hier inschrijven voor een toernooi
                        </div>
                        <div class="card-body">

                            @error('SignedoffTournament')
                            <span class="text-success">{{$message}}</span><br>
                            @enderror

                            {!! Form::open(['method'=>'post','route'=>['signup.store']]) !!}
                            @csrf
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    {!! Form::label('tournament','Toernooi:') !!}
                                    {!! Form::select('tournament_id',$SelectTournaments,null,["class"=>"custom-select"]) !!}
                                    <br>
                                    @error('SignedinTournament')
                                    <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                            </div>
                            {!! Form::submit("Aanmelden",['class'=>'btn btn-primary']) !!}
                            {!! Form::close() !!}
                        </div>
                    </div>
                @endif
                @if($tournaments->first() === null )
                    <div class="card">
                        <div class="card-header text-danger text-center">Aanmelden</div>
                        <div class="card-body">Er zijn momenteel geen toernooien om voor aan te melden.</div>
                    </div>
                @endif
            </div>


            <div class="col-md-10 mt-4">
                @if( $signups->first()!= null )
                    <div class="card">
                        <div class="card-header">
                            Je bent aangemeld voor de volgende toernooien
                        </div>
                        <div class="card-body">
                            <div class="m-1">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th scope="col">Toernooi</th>
                                        <th scope="col">Start datum</th>
                                        <th scope="col">Afmelden</th>
                                        <th scope="col">Scores</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($signups as $signup)
                                        <tr>
                                            <td>{{ $signup->tournament()->name }}</td>
                                            <td>{{ date_format(date_create($signup->tournament()->date),"d-m-Y") }}</td>

                                            <td>{!! Form::open(['method' => 'DELETE', 'route' => ['signup.destroy',$signup]]) !!}
                                                {!! Form::submit('Afmelden', ['class'=>'btn btn-danger','onclick' => 'return confirm("Weet je zeker dat je wilt uitschrijven?");']) !!}
                                                {!! Form::close() !!}</td>
                                            <td><a class="btn btn-primary" href="{{route('generalranking', ['tournament' => $signup->tournament()])}}">Ga naar de scores</a></td>

                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            @endif
                        </div>
                    </div>
            </div>
        </div>
    </div>
@endsection
