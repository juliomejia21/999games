<!DOCTYPE html>
<html>
<head>
    <title>Toernooi eind resultaat</title>
</head>
<body>
    <p>Dit is de het eind resultaat voor het {{ $tournament->name }} tournament</p>

    <a href="{{route('generalranking', $tournament->id)}}">Link naar volledige resultaten</a>
</body>
</html>
