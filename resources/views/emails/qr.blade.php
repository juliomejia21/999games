<!DOCTYPE html>
<html>
<head>
    <title>{{ $qr['tournament'] }}'s token.</title>
</head>
<body>
    <p>Dit is de qr code voor het {{ $qr['tournament'] }}</p>
    <img src="data:image/png;base64, {{ $message->embedData(base64_decode($qr['qrbash']), "QR code") }}">
</body>
</html>
