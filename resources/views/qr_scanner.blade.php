@extends('layouts.app')

@section('content')

    <div class="container-fluid qr_scanner">
        <div class="row">
            <div class="col-12 video-class">
                <video muted playsinline id="qr-video" class=""></video>
            </div>
        </div>
    </div>

    <link href="{{ asset('css/qrScanner.css') }}" rel="stylesheet">

@endsection
