@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            @if(!empty($selectedRound))
                <div class="col-md-12">
                    <h1 class="text-center">{{$tournament->name}}</h1>
                    <h3 class="text-center">
                        <div class="dropdown">
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Ronde: {{$selectedRound->name}}
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                @foreach($allRounds as $round)
                                    <a class="dropdown-item" href="{{route('history.index', ['tournament' => $tournament->id, 'round' => $round->id])}}">Ronde: {{$round->name}}</a>
                                @endforeach
                            </div>
                        </div>
                    </h3>

                    <div class="card-columns mt-5">
                        @foreach($usedTables as $usedTable)
                            <div class="card">
                                <div class="card-header"><h5
                                        class="text-center card-title">{{$usedTable->name}}</h5>
                                </div>
                                <div class="card-body">
                                    <ul class="list-group list-group-flush">
                                        @foreach($users as $user)
                                            @if($user->pivot->table_id === $usedTable->id)
                                                <li style="{{Auth::user()->id === $user->id ? "background-color: #ededed" : ""}}"
                                                    class="list-group-item">{{$user->name}}<span
                                                        class="float-right">{{$user->scores()->where('round_id', $selectedRound->id)->first()->score}}</span>
                                                </li>
                                            @endif
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            @else
                <div class="col-md-8">
                    <div class="card mt-5">
                        <div class="card-header text-center text-danger">Ronde niet gevonden</div>
                        <div class="card-body">Deze ronde is nog niet gemaakt</div>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection
