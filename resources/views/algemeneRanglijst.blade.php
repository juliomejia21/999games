<script src="{{asset('js/app.js')}}"></script>
<script src="{{asset('js/algemeneranglijst.js')}}"></script>
@extends('layouts.app')
@section('content')
    <form method="get" action="{{ route('generalranking', $tournament ?? '') }}">
    @csrf
    <!-- begin Ranglijst-->
        <div class="container" id="maincontainer">
            <div class="row">
                <div class="col-lg-8 col-md-8 col-sm-6 col-xs-6">

                    <table class="table grid table  table-bordered text-center">
                        <thead>
                        <tr>
                            <th class="table-success">Ranglijst</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr class="breakrow">
                            <td class="text-secondary small bg-light">Klik hier om de table in-uitklappen</td>
                        </tr>
                        <tr>
                            {{--                            <td class="table-info">{{Auth()->user()->name}}</td>--}}
                        </tr>
                        @php
                            $place = 0;
                        @endphp
                        @foreach($generalRankingList as $grl)
                            @php
                                $place++;
                            @endphp
                            <tr>
                                <td style="{{$grl->id === \Illuminate\Support\Facades\Auth::id() ? "background-color: #ededed" : ""}}">{{$place}}. - {{$grl->name}} - {{$grl->score}}</td>
                            </tr>
                            <tr>
                            </tr>
                        @endforeach
                        <tr class="breakrow"></tr>
                        </tbody>
                    </table>
                </div>

                <!-- Einde Ranglijst-->

                <!-- begin Score per ronde-->
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                    <div class="show-qr">
                        <table class="table  table-striped table-bordered text-center">
                            <thead>
                                <tr>
                                    <th class="table-success">Qr Code</th>
                                </tr>
                            </thead>
                            <tbody>
                            <tr class="breakrow">
                                <td class="text-secondary small bg-light">Klik hier om de qr code in-uitklappen</td>
                                <tr>
                                    <td>{!! QrCode::size(300)->generate( $qr_token ) !!}</td>
                                </tr>
                            </tr>
                        </table>

                    </div>
                    <table class="table  table-striped table-bordered text-center">
                        <thead>
                        <tr>
                            <th class="table-success">User score per ronde</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr class="breakrow">
                            <td class="text-secondary small bg-light">Klik hier om de table in-uitklappen</td>
                        </tr>
                        @foreach($UserRoundScore as $scores)
                            <tr>
                                <td>Ronde {{ $scores->round_id}}: {{ $scores->weight}}</td>
                            </tr>
                        @endforeach
                        <tr class="breakrow"></tr>
                        </tbody>
                    </table>
                    <!-- Eind score per ronde-->

                </div>
            </div>
        </div>

@endsection



