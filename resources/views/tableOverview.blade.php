@extends('layouts.app')

@section('content')

    <link href="{{ asset('css/tableOverview.css') }}" rel="stylesheet">

    <button class="btn btn-xs btn-outline-primary" id="back-to-search" title="Go to the search bar">Back to search</button>

    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="md-form mb-3 mt-0 text-center">
                    <form class="search-form" id="search-names">
                        <input ata-provide="typeahead" class="form-control typeahead border-primary"  id="search-box" type="text" placeholder="Firstname lastname" aria-label="Search" name="search">
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            @if($tables && $users != "[]")
                @foreach($tables as $table)
                    <div class="col-md-4 col-xs-12 table-mapping mb-4">
                        <div class="card">
                            <div class="card-header text-center">
                                {{ $table->table_id }}
                            </div>
                            <div class="card-body">
                                <div class="list-group-flush">
                                    @foreach($users as $user)
                                        @if($user->table_id == $table->table_id)
                                            <li class="list-group-item names" id="{{ $user->name }}">{{ $user->name }}</li>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            @else
                <div class="col-12 text-center">
                    <h2>Er zijn nog geen spelers toegevoegd aan de tafels voor de komende ronde.</h2>
                </div>
            @endif
        </div>
    </div>

@endsection
