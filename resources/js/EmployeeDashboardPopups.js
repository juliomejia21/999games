import Swal from 'sweetalert2';
import axios from 'axios';

export default function() {
    if(document.querySelector('.js-stop-timer')) {
        const $stopTimerButton = document.querySelectorAll('.js-stop-timer');

        $stopTimerButton.addEventListener('click', (e) => {
            e.preventDefault();

            const value = e.currentTarget.dataset.value;
            const url = e.currentTarget.closest('.round__edit').dataset.url;

            Swal.fire({
                title: 'Let op!',
                text: 'Weet u zeker dat u de ronde vroegtijdig wilt stoppen?',
                icon: 'warning',
                confirmButtonText: 'Ja',
                showCancelButton: true,
                cancelButtonText: 'Nee',
                showCloseButton: true,
                preConfirm: () => {
                    axios.put(url, {status: value});

                    // axios.put(url, {status: value}).then((response) => {
                    //     location.reload();
                    // });
                }
            });
        });
    }

    if(document.querySelector('.js-start-round')) {
        const $startRoundButtons = document.querySelectorAll('.js-start-round');

        $startRoundButtons.forEach((button) => {
            button.addEventListener('click', (e) => {
                e.preventDefault();

                const value = e.currentTarget.dataset.value;
                const url = e.currentTarget.closest('.round__edit').dataset.url;

                Swal.fire({
                    title: 'Let op!',
                    text: 'Zit iedereen klaar?',
                    icon: 'warning',
                    confirmButtonText: 'Ja',
                    showCancelButton: true,
                    cancelButtonText: 'Nee',
                    showCloseButton: true,
                    preConfirm: () => {
                        axios.put(url, {status: value}).then((response) => {
                            // console.log(response);
                            location.reload();
                        }).catch(function (error) {
                            // console.log(error, 'err');
                        })
                    }
                })
            })
        })
    }
}
