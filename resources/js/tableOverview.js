var back_to_search_btn = document.getElementById("back-to-search");
let tournament = getTournamentId();
var names_list = [];
$highlight = "";

function getTournamentId() {
    return location.pathname.split('/')[1];
}

fetch("/" + tournament + "/overview/players", {method: 'get'})
    .then(function (response) {
        return response.json();
    })
    .then(function (json) {
        fillPlayerList(json);
    })
    .then(function () {

        var Bloodhound = require('bloodhound-js');
        require("typeahead.js/dist/typeahead.jquery.min.js");


        var Bloodhound = require('bloodhound-js');
        var names = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.whitespace,
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            local: names_list
        });

        $('#search-names .typeahead').typeahead({
                hint: true,
                highlight: true,
                minLength: 1
            },
            {
                name: 'names',
                source: names,
                templates: {
                    empty: [
                        '<div class="empty-message, tt-suggestion">',
                        '<p>Name not found.</p>',
                        '</div>'
                    ].join('\n')
                }
            }
        );
    });

function fillPlayerList(playerList) {
    playerList.forEach(function (player) {
        names_list.push(player['name']);
    });
}

$('#search-names .typeahead').on('typeahead:selected' || 'typeahead:autocomplete', function (evt, item) {

    if ($highlight == "") {
        $highlight = document.getElementsByClassName("highlight");
    } else {
        $highlight[0].classList.remove("highlight");
    }

    document.getElementById(item).closest('.table-mapping').scrollIntoView();
    document.getElementById(item).closest('.card').classList.add("highlight");

});

window.onscroll = function () {
    scrollFunction()
};

function scrollFunction() {
    if (document.body.scrollTop > 100 || document.documentElement.scrollTop > 100) {
        back_to_search_btn.style.display = "block";
    } else {
        back_to_search_btn.style.display = "none";
    }
}

$(document).on('click', '#back-to-search', (e) => { //replaces function book()
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
});
