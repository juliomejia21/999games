require('./bootstrap');

if(document.getElementById("table")){
    window.toastr = require("toastr");
    addEventListener("DOMContentLoaded",(event) => {
        require('./scoreSelect');
    })
}

if(document.getElementsByClassName("typeahead")){
    addEventListener("DOMContentLoaded",(event) => {
        require('./tableOverview');
    })
}

if(document.querySelector('#timer')) {
    import("./countdown.js").then(module => {
        module.default();
    });
}

if(document.querySelector('#qr-video')) {
    import("./qrScanner").then(module => {
        module.default();
    });
}

// document.querySelector('.timer__stop').addEventListener('click', (e) => {
//     const url = e.currentTarget.dataset.url;
//
//     console.log(url, 'wtf');
//
//     axios.post(url);
// });

if(document.querySelector('.round__edit')) {
    import("./EmployeeDashboardPopups.js").then(module => {
        module.default();
    });
}
