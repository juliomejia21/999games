import QrScanner from 'qr-scanner';
QrScanner.WORKER_PATH = '/js/qr-scanner-worker.min.js';

import Swal from 'sweetalert2';

const video = document.getElementById('qr-video');

export default function () {

    let tournament = getTournamentId();
    var audio = new Audio('/media/qr_succesfull_scan_sound_effect.mp3');

    function getTournamentId() {
        return location.pathname.split('/')[1];
    }

    function sleep(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }

    async function setResult(result) {
        axios.patch('/' + tournament + '/scanner/update', {qr_token: result})
            .then(response => {
                if (response.data === 0) {
                    audio.play();
                    Swal.fire({
                        icon: 'success',
                        title: 'U bent ingechecked!',
                        showConfirmButton: false,
                        timer: 3000
                    });
                } else if (response.data === 1){
                    Swal.fire({
                        icon: 'error',
                        title: 'U bent al ingechecked!',
                        showConfirmButton: false,
                        timer: 1500
                    });
                }
            })
            .catch(function (error) {
                window.location = "/login";
            });
        await sleep(5000);
    }

// ####### Web Cam Scanning #######

    const scanner = new QrScanner(video, result => setResult(result));

    scanner.start();
};
