const formDOM = document.getElementsByClassName("player-scores")[0];
const tableDOM = document.getElementById("table");
tableDOM.addEventListener("change", getPlayerNames);

toastr.options.progressBar = true;
toastr.options.closeButton = true;

function getTournamentId() {
    return location.pathname.split('/')[1];
}

function getPlayerNames() {
    let table = tableDOM.value;
    let tournament = getTournamentId();
    if (table !== "") {
        fetch("/" + tournament + "/employee/" + table + "/round/players", {method: 'get'})
            .then(function (response) {
                if (response.status === 200) {
                    return response.json();
                } else {
                    toastr.error("Something went wrong while retrieving the players. Please try again later.","Error");
                }
            })
            .then(function (json) {
                InsertPlayers(json);
            })
    }
    else{
        clearPlayerDOM();
    }
}

function clearPlayerDOM() {
    let DOMList = Array.from(document.getElementsByClassName("player-score-group"));
    DOMList.forEach(function (element) {

        element.remove();

    });
}

function InsertPlayers(playerList) {
    clearPlayerDOM();
    playerList.forEach(function (player) {
        let inputGroupDOM = document.createElement("div");
        inputGroupDOM.classList.add("form-group", "player-score-group");
        formDOM.appendChild(inputGroupDOM);
        let labelDOM = document.createElement("label");
        let inputDOM = document.createElement("input");
        inputGroupDOM.appendChild(labelDOM);
        inputGroupDOM.appendChild(inputDOM);
        labelDOM.classList.add("label");
        labelDOM.innerText = player.name + " score:";
        inputDOM.classList.add("form-control");
        inputDOM.type = "number";
        inputDOM.name = "players[" + player.id + "]";
    })
}
