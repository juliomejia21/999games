export default function () {
    var countDownDate = new Date().getTime();

    var x = setInterval(function () {
        const axios = require('axios');
        const timer = document.getElementById("timer");

        axios.get(timer.dataset.url)
            .then(function (response) {
                countDownDate = new Date(response.data).getTime();
            })
            .catch(function (error) {
                // handle error
                console.log(error);
            });

        var now = new Date().getTime();

        var distance = countDownDate - now;

        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);
        minutes = (minutes < 10 ? '0' + minutes : minutes);
        seconds = (seconds < 10 ? '0' + seconds : seconds);
        if (minutes >= 45) {
            timer.innerHTML =
                "Game begint in: " + (seconds + (minutes * 60));
        } else {
            timer.innerHTML =
                (hours > 0 ? hours + ':' : '') + minutes + ":" + seconds;
        }
        if (distance < 0) {
            timer.innerHTML = "TIJD IS OVER";
        }
    }, 1000);
};