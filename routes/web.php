<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'WelcomeController@show')->name('welcome');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['role:speler']], function () {
        Route::prefix("tournament")->group(function () {
            Route::resource('signup', 'TournamentUserController');
    });
});
Route::get('tournament/employee/dashboard', function (){
    return redirect()->route('employee.dashboard',['tournament' => \App\Tournament::first()]);
});
Route::group(['prefix' => 'tournaments/{tournament}', 'middleware' => 'role:medewerker|admin'], function() {
    Route::get('/employee/dashboard', 'EmployeesController@index')->name('employee.dashboard');

    Route::get('rounds', 'RoundController@store')->name('round.store');
    Route::put('rounds/{round}', 'RoundController@update')->name('round.update');
    Route::get('rounds/{round}', 'RoundController@show')->name('round.show');
});

Route::group(['middleware' => ['role:admin']], function () {
    Route::resource('tournament', 'TournamentController')->only([
        'create', 'store'
    ]);

});

// Table overview
Route::get('{tournament}/overview', 'TableOverview@show')->name('show.tables');

Route::get('/{tournament}/overview/players', function ($tournament) {

    $latestRound = \App\Round::where('tournament_id', '=',  $tournament)->orderBy('name' ,'desc')->get('id')->first();

    $users = \App\UserRoundTable::join('tournament_user', 'user_round_table.user_id', '=', 'tournament_user.user_id')
        ->join('users', 'tournament_user.user_id', '=', 'users.id')
        ->select('users.name', 'user_round_table.table_id')
        ->where([['user_round_table.round_id', '=', $latestRound->id], ['tournament_user.tournament_id', '=', $tournament], ['tournament_user.status', '=', 1]])
        ->get();

    return $users;

});

//New Route naming.
Route::prefix("tournament/{tournament}")->group(function () {
    Route::prefix("round")->group(function () {
        Route::prefix("{round}")->group(function () {
            Route::resource("history", "UserRoundHistoryController");
        });
    });
});

Route::prefix("{tournament}")->group(function () {
    Route::get('/timer', 'TournamentController@timer')->name('timer');
    Route::get('/gettime', 'TournamentController@gettime')->name('gettime');
    Route::get('/stoptime', 'TournamentController@gettime')->name('stoptime');

    Route::group(['middleware' => ['role:speler']], function () {
        Route::get('/generalranking', 'GeneralRankingController@index')->name('generalranking');

    });
    Route::group(['middleware' => ['role:medewerker|admin']], function () {
        Route::resource('/scanner', 'QrScannerController');
        Route::prefix("employee")->group(function () {
            Route::resource("signups", "TournamentUserEmployeeController");
            Route::resource("score", "ScoreController")->only(['create', 'store']);
            Route::post("score/mail", "ScoreController@mail")->name('score.mail');


            //JS Fetch route
            Route::get('/{table}/round/players', function (\App\Tournament $tournament, \App\Table $table) {

                $table = $tournament->rounds()->orderByDesc("id")->first()->tables()->findOrFail($table->id);
                $users = $table->users(\App\Round::find($table->pivot["round_id"]))->get();

                return $users;

            })->name("round.players");
        });
    });
});
// Audio file path

Route::get('media/{filename}', function ($filename) {
    $path = storage_path('app/public/audio/' . $filename);

    if(!File::exists($path)) abort(404);

    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);

    return $response;
});
