<?php $task = isset($task) ? $task : null; ?>
<?php $path = isset($path) ? $path : null; ?>
<?php $branch = isset($branch) ? $branch : null; ?>
<?php $gitUrl = isset($gitUrl) ? $gitUrl : null; ?>
<?php $username = isset($username) ? $username : null; ?>
<?php $__container->servers(['web' => 'wessel@adsd2019.clow.nl']); ?>

<?php
if !empty($username) then
$gitUrl = 'git@ssh.dev.azure.com:v3/ictwf/2019_ADSD_Project_Frameworks_Team_05/2019_ADSD_Project_Frameworks_Team_05';
$branch = !empty($branch) ? $branch : 'master';
$path = '/home/wessel/public_html/999games';
else
echo $task;
exit; /*Or Do any other processing*/
fi
?>

<?php $__container->startTask('deploy:cold'); ?>
cd <?php echo $path; ?>

git init
git remote add origin <?php echo $gitUrl; ?>

git pull origin <?php echo $branch; ?>

composer install
npm install
cp .env.example .env
php artisan key:generate
php artisan storage:link
cd storage
chmod 777 -R .
<?php $__container->endTask(); ?>
<?php $__container->startTask('deploy'); ?>
cd <?php echo $path; ?>

git pull origin <?php echo $branch; ?>

composer install
npm install
<?php $__container->endTask(); ?>
<?php $__container->startTask('migrate'); ?>
cd <?php echo $path; ?>

php artisan migrate --seed
<?php $__container->endTask(); ?>
<?php $__container->startTask('migrate:fresh'); ?>
cd <?php echo $path; ?>

php artisan migrate:fresh --seed
<?php $__container->endTask(); ?>